enum Value {
    Ace("A"),
    Two("2"), 
    Three("3"), 
    Four("4"), 
    Five("5"), 
    Six("6"), 
    Seven("7"), 
    Eight("8"), 
    Nine("9"),
    Ten("10"), 
    OneEyedJack("anti-wild"),
	TwoEyedJack("wild"),
    Queen("Q"),
	King("K"),
    BONUS("bonus"),
    X("XXXXXXXX"),
    O("OOOOOOOO");
	
	private String name;

	
	Value(String name) {
        this.name = name;	
    }
	public String getName () {
		return name; 
	} 

}