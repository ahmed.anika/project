public class Player {
    private Card[] hand;
    final int playerHandSize = 7;
    int handPointer;
    

    public Player(int playerHandSize) {
        this.hand = new Card[playerHandSize];
        this.handPointer = 0;
    }

    public void displayHand() {
        for (int i = 0; i < playerHandSize; i++) {
            System.out.println("card " + (i+1) + ": " + hand[i]);
        }
    }

    public void populateHand(DynamicCardArray dynamicCardArray) {
        dynamicCardArray.shuffleCards();
        for (int i = 0; i < playerHandSize; i++) {
            hand[i] = dynamicCardArray.drawTopCard();
            dynamicCardArray.getPointer(); 
            handPointer++;
        }
    }

    public Card[] getHand() {
        return this.hand;
    }

    public void removeCard(int n) {
		this.hand[n] = null;
		
	}

	public void addCard(DynamicCardArray dynamicCardArray, int n){
		this.hand[n] = dynamicCardArray.drawTopCard();
	}
}