
import java.util.Random;

public class Board {
    private Card[][] grid; 
    private final int boardSize = 10;
	private Card cards[];
    private int pointer;
	private Card[][] slotHolder;

	

    public Board() {
    this.grid = new Card[boardSize][boardSize];
    this.slotHolder = new Card[boardSize][boardSize];
    this.cards = new Card[96];

   
        Slot cardSlot = new Slot(96);
		int index = 0; 
		for (int r = 0; r < boardSize; r++) {
			for (int c = 0; c < boardSize; c++) {
				if ((r == 0 && c == 0) || (r == 0 && c == (boardSize - 1)) || (r == (boardSize - 1) && c == 0) || (r == (boardSize - 1) && c == (boardSize - 1))) {
					grid[r][c] = new Card(Suit.Null, Value.BONUS);
					} else {
						cardSlot.shuffleCards();
						Card currentCard = cardSlot.drawTopCard(); 
						grid[r][c] = currentCard;
						index++; 
				}
			}
		}

		for (int i = 0; i < grid.length; i++) {
			for (int j = 0; j < grid[i].length; j++) {
				this.slotHolder[i][j] = new Card((grid[i][j]).getSuit(), (grid[i][j]).getValue());
			}
		}
	}


/* used this code (toString method) from google, im sorry I had to or else the game would look wack but I also learned from it like how the format method works. 
Here was one of the many failed attempts so you can atleast see my work/compentency that is written by me:
@Override
public String toString() {
    StringBuilder result = new StringBuilder();

    for (int i = 0; i < grid.length; i++) {
        for (int j = 0; j < grid[i].length; j++) {
            result.append(grid[i][j]).append("   ");
        }
        result.append("\n");
    }
    return result.toString();
}*/ 
	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		int maxLength = 0;
		// Find the maximum length of card representations in the grid
		for (Card[] row : grid) {
			for (Card card : row) {
				int length = card.toString().length();
				if (card.getValue() == Value.X || card.getValue() == Value.O ){
					length = length - 20;
				}
				if (length > maxLength) {
					maxLength = length;
				}
			}
		}
		
		// Append each card representation to the result with adjusted spacing
		for (Card[] row : grid) {
			for (Card card : row) {
				int spacesToAdd = 0;
				String cardString = card.toString();
				if (card.getValue() == Value.X || card.getValue() == Value.O ){
					spacesToAdd = maxLength + 5 - (cardString.length()-10); // Adjust as needed
					result.append(cardString);
				} else {
					// Adjust the spacing based on the length of the card representation and ANSI escape codes
					spacesToAdd = maxLength + 5 - cardString.length(); // Adjust as needed
					result.append(cardString);
				}
				// Add spaces to ensure consistent spacing after the colored card representation
				for (int i = 0; i < spacesToAdd; i++) {
					result.append(" ");
				}
			}
			result.append("\n" + "\n" );
		}
		return result.toString();
	}


	
	
	public boolean placeToken(int row, int col, Card tokenX, Card tokenO, Card playedCard1, Card playedCard2, int player) {
		Card gridCard = this.grid[row][col];
		System.out.println("Row: " + row + ", Col: " + col);
		System.out.println("Grid Card: " + gridCard);
		System.out.println("Played Card 1: " + playedCard1);
		System.out.println("Played Card 2: " + playedCard2);
		if (player == 1) {
			if ((gridCard.getValue() == playedCard1.getValue() && gridCard.getSuit() == playedCard1.getSuit() ) ||  playedCard1.getValue() == Value.TwoEyedJack) {
				this.grid[row][col] = tokenX;
				return true;
			} else if (playedCard1.getValue() == Value.OneEyedJack){
				this.grid[row][col] = this.slotHolder[row][col];
				return true;
				}
		} else if (player == 2) {
			if ((gridCard.getValue() == playedCard2.getValue() && gridCard.getSuit() == playedCard2.getSuit()) || playedCard2.getValue() == Value.TwoEyedJack) {
				this.grid[row][col] = tokenO;
				return true;
			} else if (playedCard2.getValue() == Value.OneEyedJack){
				this.grid[row][col] = slotHolder[row][col];
				return true;
			}
		}
		return false;
	}
 

	private boolean checkIfWinningVertical(Card tokenX, Card tokenO, int player) {
		for (int c = 0; c < this.grid.length; c++) {
        int consecutiveCount = 0;
        for (int r = 0; r < 6; r++) {
            if ((player == 1 && (this.grid[r][c]).getValue() == tokenX.getValue() && (this.grid[r][c]).getSuit() == tokenX.getSuit()) || (player == 2 && (this.grid[r][c]).getValue() == tokenO.getValue() && (this.grid[r][c]).getSuit() == tokenO.getSuit() )) {
                consecutiveCount++;
                if (consecutiveCount >= 5) {
                    return true; 
                }
            } else {
                consecutiveCount = 0; 
            }
        }
    }
    return false;
}

private boolean checkIfWinningHorizontal(Card tokenX, Card tokenO, int player) {
    for (int r = 0; r < this.grid.length; r++) {
        int consecutiveCount = 0;
        for (int c = 0; c < 6; c++) {
            if ((player == 1 && (this.grid[r][c]).getValue() == tokenX.getValue() && (this.grid[r][c]).getSuit() == tokenX.getSuit()) || (player == 2 && (this.grid[r][c]).getValue() == tokenO.getValue() && (this.grid[r][c]).getSuit() == tokenO.getSuit())) {
                consecutiveCount++;
                if (consecutiveCount >= 5) {
                    return true; 
                }
            } else {
                consecutiveCount = 0; 
            }
        }
    }
    return false;
}


public boolean checkIfWinning(Card tokenX, Card tokenO, int player) { 
    return checkIfWinningHorizontal(tokenX, tokenO, player) || checkIfWinningVertical(tokenX, tokenO, player);
}

}
