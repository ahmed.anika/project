import java.util.Scanner;
import java.util.InputMismatchException;

public class SequenceGame {
    public static void main(String[] args) {
        Board board = new Board();
        Board slot = new Board();
        Card[] playerCards = new Card[7];
        Card playedCard1 = null;
        Card playedCard2 = null;
        Card tokenO = new Card(Suit.Null, Value.O);
        Card tokenX = new Card(Suit.Null, Value.X);
        int indexCardPlaced = 0;

        DynamicCardArray deck = new DynamicCardArray(104);
        Player playerOne = new Player(7);
        int player = 1;
        Player playerTwo = new Player(7);

        boolean gameOver = false;

        playerOne.populateHand(deck);
        playerTwo.populateHand(deck);

        while (!gameOver) {
            System.out.println(board);
            if (player == 1) {
                playerOne.displayHand();
            } else {
                playerTwo.displayHand();
            }
            Scanner scanner = new Scanner(System.in);
            try {
                System.out.print("Player " + player + " would you like to use bonus (b) slot or play a card (c)? ");
                String BC = scanner.nextLine().toUpperCase();
                if (!BC.equals("B") && !BC.equals("C")) {
                    throw new IllegalArgumentException();
                }
                if (BC.equals("C")) {
                    System.out.print("Player " + player + ", which card would you like to place? (1-7): ");
                    indexCardPlaced = scanner.nextInt() - 1;
                    if (indexCardPlaced < 0 || indexCardPlaced >= 7) {
                        throw new IllegalArgumentException();
                    }
                    if (player == 1) {
                        playerCards = playerOne.getHand();
                        playedCard1 = playerCards[indexCardPlaced];
                    } else {
                        playerCards = playerTwo.getHand();
                        playedCard2 = playerCards[indexCardPlaced];
                    }
                } else {
                    if (player == 1) {
                        playedCard1 = new Card(Suit.Null, Value.BONUS);
                    } else {
                        playedCard2 = new Card(Suit.Null, Value.BONUS);
                    }
                }

                System.out.print("Player " + player + ", enter the row (1-10): ");
                int col = scanner.nextInt() - 1;
                if (col < 0 || col >= 10) {
                    throw new IllegalArgumentException();
                }
                System.out.print("Player " + player + ", enter the column (1-10): ");
                int row = scanner.nextInt() - 1;
                if (row < 0 || row >= 10) {
                    throw new IllegalArgumentException();
                }
                boolean validInput = board.placeToken(row, col, tokenX, tokenO, playedCard1, playedCard2, player);
                if (!validInput) {
                    System.out.println("Invalid input! Your card doesn't match the card slot you chose.");
                } else if (board.checkIfWinning(tokenX, tokenO, player)) {
                    System.out.println("Player " + player + " is the winner!");
                    gameOver = true;
                } else {
                    if (BC.equals("C")) {
                        if (player == 1) {
                            playerOne.removeCard(indexCardPlaced);
                            playerOne.addCard(deck, indexCardPlaced);
                        } else {
                            playerTwo.removeCard(indexCardPlaced);
                            playerTwo.addCard(deck, indexCardPlaced);
                        }
                    }
                    player++;
                    if (player > 2) {
                        player = 1;
                    }
                }
            } catch (IllegalArgumentException | InputMismatchException e) {
                System.out.println("Invalid input :(");
            }
        }
    }
}
