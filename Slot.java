import java.util.Random;

public class Slot {
    int size = 96;
	private Card[] cards;
	private int pointer;

    public Slot(int size) {
        this.cards = new Card [size];
        this.pointer = 0;
        int i = 0;
        for (Value v : Value.values()) {
			for (Suit s : Suit.values()) {
				if (v != Value.BONUS && s != Suit.Null && v != Value.OneEyedJack && v != Value.TwoEyedJack && v != Value.O && v != Value.X){
					cards[i] = new Card(s, v);
					this.pointer++;
					i++; 
                    cards[i] = new Card(s, v);
					this.pointer++;
					i++; 
					
				}
			}
		}

    }

    public int getPointer(){
		return this.pointer;
	}
	
    public Card drawTopCard(){
		this.pointer--;
		return cards[this.pointer];
	}

    public void shuffleCards(){
		java.util.Random rng = new java.util.Random();
		for (int i = 0; i < this.pointer; i++) {
			int randomIndex = rng.nextInt(this.pointer);
			Card shuffledCards = this.cards[randomIndex];
			this.cards[randomIndex] = this.cards[i];
			this.cards[i] = shuffledCards;
		}
	}
}

