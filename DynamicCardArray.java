import java.util.Random;
// 104 cards in total in deck but 96 in board
public class DynamicCardArray{
	int size = 104;
	private Card[] cards;
	private int pointer;
	
	
	public DynamicCardArray(int size) {
		this.cards = new Card[size];
		this.pointer = 0;

		int i = 0;
        for (Value v : Value.values()) {
			for (Suit s : Suit.values()) {
				if (v != Value.BONUS && v != Value.X && v != Value.O && s != Suit.Null){
					cards[i] = new Card(s, v);
					this.pointer++;
					i++; 
					if (v != Value.OneEyedJack && v != Value.TwoEyedJack) {
						cards[i] = new Card(s, v); // Add the card again
						this.pointer++;
						i++; 
					}
				}
			}
		}

	}
	
	public int getPointer() {
		return this.pointer;
	}
	
	public Card drawTopCard(){
		this.pointer--;
		return cards[this.pointer];
	}
	
	public void shuffleCards(){
		java.util.Random rng = new java.util.Random();
		for (int i = 0; i < this.pointer; i++) {
			int randomIndex = rng.nextInt(this.pointer);
			Card shuffledCards = this.cards[randomIndex];
			this.cards[randomIndex] = this.cards[i];
			this.cards[i] = shuffledCards;
		}
	}

	
}
	