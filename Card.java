public class Card {
    private Suit suit;
    private Value value;
    
   

    
    public Card (Suit suit, Value value) {
        this.suit = suit;
        this.value = value;

    }


    public Suit getSuit() {
		return this.suit;
	}

    public Value getValue() {
		return this.value;
	}

    
	
     public String toString() {
        if (value == Value.BONUS){
            return value.getName();
        } else if (value == Value.O){
            return "\u001B[32m" + value.getName() + "\u001B[37m ";
        } else if (value == Value.X ){
            return "\u001B[35m" + value.getName() + "\u001B[37m";
        } else {
            return value.getName() + "-" + suit;
        }
            
        
    }
}
